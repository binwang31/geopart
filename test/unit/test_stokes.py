import pytest

import numpy as np
import geopart.stokes
import dolfin
import dolfin_dg as dg
from ufl import sym, grad, Identity, div, dx


class MMS(geopart.stokes.StokesModel):
    def __init__(self, comm, nx, ny):
        self.mesh = dolfin.RectangleMesh.create(
            comm, [dolfin.Point(-1.0, -1.0), dolfin.Point(1.0, 1.0)],
            [nx, ny], dolfin.CellType.Type.triangle, "left/right")

        u_soln_code = ("-(x[1]*cos(x[1]) + sin(x[1]))*exp(x[0])",
                       "x[1] * sin(x[1]) * exp(x[0])")
        p_soln_code = "2.0 * exp(x[0]) * sin(x[1]) " \
                      "+ 1.5797803888225995912 / 3.0"

        u_soln = dolfin.Expression(u_soln_code, degree=4, domain=self.mesh)
        p_soln = dolfin.Expression(p_soln_code, degree=4, domain=self.mesh)
        f = -div(2 * sym(grad(u_soln)) - p_soln * Identity(2))

        self.u_soln = u_soln
        self.p_soln = p_soln

        super().__init__(eta=1, f=f)

    @staticmethod
    def generate_facet_function(mesh):
        ff = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
        dolfin.CompiledSubDomain(
            "near(x[0], -1.0) or near(x[1], -1.0)").mark(ff, 1)
        dolfin.CompiledSubDomain(
            "near(x[0], 1.0) or near(x[1], 1.0)").mark(ff, 2)
        return ff

    def generate_bcs(self, ds):
        n = dolfin.FacetNormal(self.mesh)
        u_soln, p_soln = self.u_soln, self.p_soln
        gN = (2*sym(grad(u_soln))
              - p_soln*Identity(self.mesh.geometry().dim())) * n
        return [dg.DGDirichletBC(ds(1), u_soln),
                dg.DGNeumannBC(ds(2), gN)]


elements_rates = [
    (geopart.stokes.TaylorHood, 3, 2, 2, 1),
    (geopart.stokes.HDG2, 3, 2, 2, 1),
    (geopart.stokes.HDG, 2, 1, 1, None),
]


@pytest.mark.parametrize("run_data", elements_rates)
def test_stokes_convergence(run_data):
    element_class, ul2_rate, uh1_rate, pl2_rate, ph1_rate = run_data

    n_eles = [8, 16, 32]
    erroru_l2 = np.array([0.0] * len(n_eles), dtype=np.double)
    erroru_h1 = np.array([0.0] * len(n_eles), dtype=np.double)
    erroru_div = np.array([0.0] * len(n_eles), dtype=np.double)
    errorp_l2 = np.array([0.0] * len(n_eles), dtype=np.double)
    errorp_h1 = np.array([0.0] * len(n_eles), dtype=np.double)
    hsizes = np.array([0.0] * len(n_eles), dtype=np.double)

    for j, n_ele in enumerate(n_eles):
        element_cls = element_class()
        model = MMS(dolfin.MPI.comm_world, n_ele, n_ele)
        mesh = model.mesh

        # Define the variational (projection problem)
        W = element_cls.function_space(mesh)

        u_soln, p_soln = model.u_soln, model.p_soln
        ff = model.generate_facet_function(mesh)
        ds = dolfin.Measure("ds", subdomain_data=ff)

        U = element_cls.solution_variable(W)
        weak_bcs = model.generate_bcs(ds)

        # Forms Stokes
        A, b = dolfin.PETScMatrix(), dolfin.PETScVector()
        model.f = -div(
            2 * sym(grad(u_soln)) - p_soln * Identity(mesh.geometry().dim()))
        element_cls.solve_stokes(W, U, (A, b), weak_bcs, model)

        # Particle advector
        hmin = dolfin.MPI.min(mesh.mpi_comm(), mesh.hmin())

        # Transfer the computed velocity function and compute functionals
        uh, ph = element_cls.get_velocity(U), element_cls.get_pressure(U)
        erroru_l2[j] = dolfin.errornorm(u_soln, uh, "l2", degree_rise=1)
        erroru_h1[j] = dolfin.errornorm(u_soln, uh, "h1", degree_rise=1)
        erroru_div[j] = dolfin.assemble(div(uh) ** 2 * dx) ** 0.5
        if ph is not None:
            errorp_l2[j] = dolfin.errornorm(p_soln, ph, "l2", degree_rise=1)
            errorp_h1[j] = dolfin.errornorm(p_soln, ph, "h1", degree_rise=1)
        hsizes[j] = hmin

    hrates = np.log(hsizes[:-1] / hsizes[1:])
    ratesu_l2 = np.log(erroru_l2[:-1] / erroru_l2[1:]) / hrates
    ratesu_h1 = np.log(erroru_h1[:-1] / erroru_h1[1:]) / hrates

    dolfin.info("errors u l2: %s" % str(erroru_l2))
    dolfin.info("rates u l2: %s" % str(ratesu_l2))
    dolfin.info("rates u h1: %s" % str(ratesu_h1))
    assert np.all(np.abs(ratesu_l2 - ul2_rate) < 0.1)
    assert np.all(np.abs(ratesu_h1 - uh1_rate) < 0.1)

    if ph is not None:
        ratesp_l2 = np.log(errorp_l2[:-1] / errorp_l2[1:]) / hrates
        ratesp_h1 = np.log(errorp_h1[:-1] / errorp_h1[1:]) / hrates
        dolfin.info("rates p l2: %s" % str(ratesp_l2))
        dolfin.info("rates p h1: %s" % str(ratesp_h1))

        assert np.all(np.abs(ratesp_l2 - pl2_rate) < 0.11)
        if ph1_rate is not None:
            assert np.all(np.abs(ratesp_h1 - ph1_rate) < 0.11)

    if isinstance(element_class, geopart.stokes.HDG):
        assert np.all(np.abs(erroru_div) < 1e-11)
