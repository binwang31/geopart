import abc
import typing
import ufl
import ufl.core.expr
import dolfin
import leopart


class CompositionScheme(abc.ABC):

    def __init__(self,
                 bounded: typing.Union[None, typing.Sequence[float]] = None,
                 periodic: typing.Union[None, dolfin.SubDomain] = None):
        """
        Parameters
        ----------
        bounded : Optional sequence of floats indicating the scalar lower and
            upper bound (lower, upper)
        periodic : Optional periodic boundary used in construction of the
            underlying composition field's function space
        """
        self.bounded = bounded
        self.periodic = periodic

    @abc.abstractmethod
    def composition_element(self, mesh: dolfin.Mesh) -> ufl.FiniteElementBase:
        """
        Parameters
        ----------
        mesh : Mesh on which the tracers are defined

        Returns
        -------
        The UFL finite element with which the underlying FE function space is
        defined.
        """
        pass

    @abc.abstractmethod
    def function_space(self) -> dolfin.FunctionSpace:
        """
        Returns
        -------
        The FE function space upon which the composition field is defined and
        into which tracer data may be projected
        """
        pass

    @abc.abstractmethod
    def project(self, phi: dolfin.Function) -> None:
        """
        Project the current state of the tracer data to the provided FE
        function.

        Parameters
        ----------
        phi : The FE function defined on the underlying composition field FE
            function space
        """
        pass


class LeastSquaresDG0(CompositionScheme):

    def __init__(self, ptcls: leopart.particles, u: dolfin.Function,
                 dt: ufl.core.expr.Expr, property_idx: int,
                 **kwargs):
        """
        Parameters
        ----------
        ptcls : LEoPart particles object
        u : Velocity function used to advect the particles (necessary in
            PDE-constrained projection)
        dt : Time step size
        property_idx : The index of the particle data in the LEoPart
            particles object
        kwargs : Passed to the superclass
        """
        mesh = ptcls.mesh()
        self.Vh = dolfin.FunctionSpace(mesh, self.composition_element(mesh))
        self.l2p = leopart.l2projection(ptcls, self.Vh, property_idx)
        self.u = u
        self.dt = dt
        super().__init__(**kwargs)

    def poly_order(self) -> int:
        return 0

    def composition_element(self, mesh: dolfin.Mesh) -> ufl.FiniteElementBase:
        return ufl.FiniteElement("DG", mesh.ufl_cell(), self.poly_order())

    def function_space(self) -> dolfin.FunctionSpace:
        return self.Vh

    def project(self, phi: dolfin.Function) -> None:
        if self.bounded in (None, False):
            self.l2p.project(phi.cpp_object())
        else:
            lb, ub = self.bounded
            self.l2p.project(phi.cpp_object(), lb, ub)


class LeastSquaresDG1(LeastSquaresDG0):

    def poly_order(self) -> int:
        return 1


class LeastSquaresDG2(LeastSquaresDG0):

    def poly_order(self) -> int:
        return 2


class LeastSquaresDG3(LeastSquaresDG0):

    def poly_order(self) -> int:
        return 3


class PDEConstrainedDG0(CompositionScheme):

    def poly_order(self):
        return 0

    def __init__(self, ptcls: leopart.particles, u: dolfin.Function,
                 dt: ufl.core.expr.Expr, property_idx: int, **kwargs):
        """
        Parameters
        ----------
        ptcls : LEoPart particles object
        u : Velocity function used to advect the particles (necessary in
            PDE-constrained projection)
        dt : Time step size
        property_idx : The index of the particle data in the LEoPart
            particles object
        kwargs : Passed to the superclass
        """

        super().__init__(**kwargs)

        self.ptcls = ptcls
        mesh = ptcls.mesh()

        self.zeta_val = 0.0 if self.bounded in (None, False) else 25.0

        T_e = ufl.FiniteElement("DG", mesh.ufl_cell(), 0)
        Wbar_e = ufl.FiniteElement("DGT", mesh.ufl_cell(), self.poly_order())

        self.W = dolfin.FunctionSpace(mesh, self.composition_element(mesh))
        T = dolfin.FunctionSpace(mesh, T_e)
        Wbar = dolfin.FunctionSpace(mesh, Wbar_e) if self.periodic is None \
            else dolfin.FunctionSpace(mesh, Wbar_e,
                                      constrained_domain=self.periodic)
        # lambda_h = dolfin.Function(T)
        self.phibar_h = dolfin.Function(Wbar)
        self.bcs = [dolfin.DirichletBC(
            Wbar, dolfin.Constant(0.), "on_boundary")] \
            if self.periodic is None else []

        FuncSpace_adv = {'FuncSpace_local': self.W, 'FuncSpace_lambda': T,
                         'FuncSpace_bar': Wbar}
        self.forms_pde_map = leopart.FormsPDEMap(ptcls.mesh(), FuncSpace_adv)
        self.u = u
        self.dt = dt
        self.property_idx = property_idx
        self.pde_projection = None

    def composition_element(self, mesh: dolfin.Mesh) -> ufl.FiniteElementBase:
        return ufl.FiniteElement("DG", mesh.ufl_cell(), self.poly_order())

    def function_space(self) -> dolfin.FunctionSpace:
        return self.W

    def project(self, phi: dolfin.Function) -> None:
        if self.pde_projection is None:
            theta = dolfin.Constant(0.5)
            forms_pde_map = self.forms_pde_map
            forms_pde = forms_pde_map.forms_theta_linear(
                phi, self.u, self.dt, theta,
                zeta=dolfin.Constant(self.zeta_val))

            pde_projection = leopart.PDEStaticCondensation(
                self.ptcls.mesh(), self.ptcls,
                forms_pde['N_a'], forms_pde['G_a'], forms_pde['L_a'],
                forms_pde['H_a'],
                forms_pde['B_a'],
                forms_pde['Q_a'], forms_pde['R_a'], forms_pde['S_a'],
                self.bcs, self.property_idx)
            self.pde_projection = pde_projection

        self.pde_projection.assemble(True, True)
        # self.pde_projection.solve_problem(self.phibar_h.cpp_object(),
        #                                   phi.cpp_object(),
        #                                   self.lambda_h.cpp_object(),
        #                                   "mumps", "default")
        self.pde_projection.solve_problem(self.phibar_h.cpp_object(),
                                          phi.cpp_object(),
                                          "mumps", "default")


class PDEConstrainedDG1(PDEConstrainedDG0):

    def poly_order(self) -> int:
        return 1


class PDEConstrainedDG2(PDEConstrainedDG0):

    def poly_order(self) -> int:
        return 2


class PDEConstrainedDG3(PDEConstrainedDG0):

    def poly_order(self) -> int:
        return 3
