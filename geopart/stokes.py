import abc
import typing

import ufl
from ufl import sym, div, grad, inner, dot, Identity

import dolfin
from dolfin import dx
import leopart

import dolfin_dg
import dolfin_dg.operators


class StokesModel:

    def __init__(self, eta=None, f=None):
        self.eta = eta
        self.f = f


class StokesElement(abc.ABC):

    @abc.abstractmethod
    def stokes_elements(self, mesh: dolfin.Mesh):
        """
        The UFL finite elements underlying the scheme.

        Parameters
        ----------
        mesh: Problem mesh
        """
        pass

    @abc.abstractmethod
    def function_space(self, mesh: dolfin.Mesh):
        """
        Generate DOLFIN function space(s) required for the complete scheme.

        Parameters
        ----------
        mesh: Problem mesh
        """
        pass

    @abc.abstractmethod
    def velocity_function_space(self, mesh: dolfin.Mesh):
        """
        Generate a function space solely for definition of the velocity
        function.

        Parameters
        ----------
        mesh: Problem mesh
        """
        pass

    def solution_variable(self, W: dolfin.FunctionSpace) -> dolfin.Function:
        """
        Given function space(s), generate DOLFIN FE function(s) into which
        the system solution may be stored.

        Parameters
        ----------
        W: Problem finite element space

        Returns
        -------
        An appropriate DOLFIN function(s)
        """
        return dolfin.Function(W)

    @abc.abstractmethod
    def forms(self, W: dolfin.FunctionSpace, U: dolfin.Function,
              weak_bcs: typing.Sequence[dolfin_dg.operators.DGBC],
              model: StokesModel):
        """
        Generate the UFL FE formulation required by the numerical scheme.

        Parameters
        ----------
        W: Complete FE space(s)
        U: Compute solution function(s)
        weak_bcs: Problem BCs
        model: Viscosity and momentum source model
        """
        pass

    @abc.abstractmethod
    def assemble(self, forms: typing.Sequence[ufl.Form],
                 mats: typing.Tuple[dolfin.PETScMatrix, dolfin.PETScVector],
                 bcs: typing.Sequence[dolfin_dg.operators.DGBC]):
        """
        Assemble the provided forms into the given linear algebra objects.

        Parameters
        ----------
        forms: UFL FE formulations
        mats: Matrices/Vectors required for the linear solution
        bcs: Problem BCs
        """
        pass

    @abc.abstractmethod
    def compute_cfl_dt(self, u_vec: dolfin.Function, hmin: float,
                       c_cfl: float):
        """
        Given the velocity function, compte an appropriate estimate for optimal
        time step size based on the CFL criterion.

        Parameters
        ----------
        u_vec: Velocity function
        hmin: Minimum spacial characteristic (e.g. smallest mesh cell size)
        c_cfl: CFL number
        """
        pass

    @abc.abstractmethod
    def get_velocity(self, U: dolfin.Function):
        """
        Given a complete system solution function(s), extract and return only
        the velocity component.

        Parameters
        ----------
        U: Complete system solution
        """
        pass

    @abc.abstractmethod
    def get_pressure(self, U: dolfin.Function):
        """
        Given a complete system solution function(s), extract and return only
        the pressure component.

        Parameters
        ----------
        U: Complete system solution
        """
        pass


class TaylorHood(StokesElement):
    """
    Implementation of the standard Taylor-Hood element.
    """

    def __init__(self):
        solver = dolfin.PETScKrylovSolver()
        dolfin.PETScOptions.set("ksp_type", "preonly")
        dolfin.PETScOptions.set("pc_type", "lu")
        dolfin.PETScOptions.set("pc_factor_mat_solver_type", "mumps")

        solver.set_from_options()

        self.solver = solver

    def stokes_elements(self, mesh: dolfin.Mesh):
        Ve = ufl.VectorElement("CG", mesh.ufl_cell(), 2)
        Qe = ufl.FiniteElement("CG", mesh.ufl_cell(), 1)
        return Ve, Qe

    def function_space(self, mesh: dolfin.Mesh):
        return dolfin.FunctionSpace(
            mesh, ufl.MixedElement(self.stokes_elements(mesh)))

    def velocity_sub_space(self, W: dolfin.FunctionSpace):
        return W.sub(0)

    def get_velocity(self, U: dolfin.Function):
        return U.sub(0)

    def get_pressure(self, U: dolfin.Function):
        return U.sub(1)

    def velocity_function_space(self, mesh: dolfin.Mesh):
        return dolfin.FunctionSpace(mesh, self.stokes_elements(mesh)[0])

    def pressure_function_space(self, mesh: dolfin.Mesh):
        return dolfin.FunctionSpace(mesh, self.stokes_elements(mesh)[1])

    def forms(self, W: dolfin.FunctionSpace, U: dolfin.Function,
              weak_bcs: typing.Sequence[dolfin_dg.operators.DGBC],
              model: StokesModel):
        u, p = ufl.split(dolfin.TrialFunction(W))
        v, q = ufl.split(dolfin.TestFunction(W))
        eta = model.eta
        f = model.f
        a = inner(2 * eta * sym(grad(u)), sym(grad(v))) * dx - p * div(
            v) * dx - q * div(u) * dx
        L = inner(f, v) * dx

        for bc in weak_bcs:
            if isinstance(bc, dolfin_dg.DGDirichletBC):
                pass
            if isinstance(bc, dolfin_dg.DGNeumannBC):
                L += dot(bc.get_function(), v) * bc.get_boundary()

        return a, L

    def assemble(self,
                 forms: typing.Sequence[typing.Union[dolfin.Form, ufl.Form]],
                 mats: typing.Sequence[typing.Union[
                                           dolfin.Vector, dolfin.Matrix]],
                 bcs: typing.Sequence[dolfin_dg.operators.DGBC]) -> None:
        a, L = forms
        A, b = mats
        V = self.velocity_sub_space(L.arguments()[0].function_space())
        strong_bcs = [dolfin.DirichletBC(V,
                                         bc.get_function(),
                                         bc.get_boundary().subdomain_data(),
                                         bc.get_boundary().subdomain_id())
                      for bc in bcs if isinstance(bc, dolfin_dg.DGDirichletBC)]
        strong_bcs += [dolfin.DirichletBC(V.sub(bc.component),
                                          dolfin.Constant(0.0),
                                          bc.get_boundary().subdomain_data(),
                                          bc.get_boundary().subdomain_id())
                       for bc in bcs
                       if isinstance(bc, dolfin_dg.DGDirichletNormalBC)]
        self.system_assembler = dolfin.SystemAssembler(a, L, strong_bcs)
        self.system_assembler.assemble(A, b)

    def solve_stokes(self, W: dolfin.FunctionSpace, U: dolfin.Function,
                     mats: typing.Sequence[typing.Union[
                                               dolfin.Vector, dolfin.Matrix]],
                     weak_bcs: typing.Sequence[dolfin_dg.operators.DGBC],
                     model: StokesModel) -> None:
        # Initial Stokes solve
        a, L = self.forms(W, U, weak_bcs, model)
        A, b = mats

        U.vector()[:] = 0.0  # Homogenise the problem
        self.assemble((a, L), (A, b), weak_bcs)

        self.solver.set_operator(A)
        self.solver.solve(U.vector(), b)

    def compute_cfl_dt(self, u_vec, hmin, c_cfl):
        max_u_vec = u_vec.vector().norm("linf")
        return c_cfl * hmin / max_u_vec


class Mini(TaylorHood):
    """
    Implementation of the equal order MINI scheme.
    """

    def stokes_elements(self, mesh: dolfin.Mesh):
        P1 = ufl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        B = ufl.FiniteElement(
            "Bubble", mesh.ufl_cell(), mesh.topology().dim() + 1)
        Ve = ufl.VectorElement(ufl.NodalEnrichedElement(P1, B))
        Qe = P1
        return Ve, Qe


class MiniP2(Mini):

    def stokes_elements(self, mesh: dolfin.Mesh):
        P1 = ufl.FiniteElement("Lagrange", mesh.ufl_cell(), 2)
        B = ufl.FiniteElement(
            "Bubble", mesh.ufl_cell(), mesh.topology().dim() + 1)
        Ve = ufl.VectorElement(ufl.NodalEnrichedElement(P1, B))
        Qe = P1
        return Ve, Qe


class P2BDG1(TaylorHood):
    """
    Implementation of the conforming enriched velocity (P2B) and nonconforming
    pressure (DG1) scheme.
    """

    def stokes_elements(self, mesh: dolfin.Mesh):
        P2 = ufl.FiniteElement("Lagrange", mesh.ufl_cell(), 2)
        B = ufl.FiniteElement(
            "Bubble", mesh.ufl_cell(), mesh.topology().dim() + 1)
        Ve = ufl.VectorElement(ufl.NodalEnrichedElement(P2, B))
        Qe = ufl.FiniteElement("DG", mesh.ufl_cell(), 1)
        return Ve, Qe


class P2DG0(TaylorHood):
    """
    Implementation of the suboptimal conforming velocity (P2) and nonconforming
    piecewise constant pressure (DG0) scheme.
    """

    def stokes_elements(self, mesh: dolfin.Mesh):
        Ve = ufl.VectorElement("Lagrange", mesh.ufl_cell(), 2)
        Qe = ufl.FiniteElement("DG", mesh.ufl_cell(), 0)
        return Ve, Qe


class CR(TaylorHood):
    """
    Implementation of the nonconforming Crouzeix-Raviart scheme.
    """

    def stokes_elements(self, mesh: dolfin.Mesh):
        Ve = ufl.VectorElement("Crouzeix-Raviart", mesh.ufl_cell(), 1)
        Qe = ufl.FiniteElement("Discontinuous Lagrange", mesh.ufl_cell(), 0)
        return Ve, Qe


class DG(TaylorHood):
    """
    Implementation of the nonconforming Crouzeix-Raviart scheme.
    """

    def stokes_elements(self, mesh: dolfin.Mesh):
        Ve = ufl.VectorElement("DG", mesh.ufl_cell(), 1)
        Qe = ufl.FiniteElement("DG", mesh.ufl_cell(), 0)
        return Ve, Qe

    def forms(self, W: dolfin.FunctionSpace, U: dolfin.Function,
              weak_bcs: typing.Sequence[dolfin_dg.operators.DGBC],
              model: StokesModel):
        u, p = ufl.split(U)
        v, q = ufl.split(dolfin.TestFunction(W))

        eta = model.eta

        def F_v(u, grad_u):
            return 2 * eta * sym(grad_u) - p * Identity(2)

        self.F_v = F_v

        stokes_op = dolfin_dg.StokesOperator(W.mesh(), W, weak_bcs, F_v)
        F = stokes_op.generate_fem_formulation(u, v, p, q)
        F -= inner(model.f, v) * dx

        stokes_nitsche = dolfin_dg.StokesNitscheBoundary(
            F_v, u, p, v, q, delta=-1)
        for bc in weak_bcs:
            if isinstance(bc, dolfin_dg.DGDirichletNormalBC):
                F += stokes_nitsche.slip_nitsche_bc_residual(
                    bc.get_function(),
                    dolfin.Constant((0.0, 0.0)),
                    bc.get_boundary()
                )

        a = ufl.derivative(F, U)
        L = -F

        return a, L


class DG2(DG):

    def stokes_elements(self, mesh: dolfin.Mesh):
        Ve = ufl.VectorElement("DG", mesh.ufl_cell(), 2)
        Qe = ufl.FiniteElement("DG", mesh.ufl_cell(), 1)
        return Ve, Qe


class HDG(StokesElement):
    """
    Implementation of the hybrid discontinuous Galerkin scheme.
    """

    k = 1

    def stokes_elements(self, mesh: dolfin.Mesh):
        k = self.k
        W_e_2 = ufl.VectorElement("DG", mesh.ufl_cell(), k)
        Q_E = ufl.FiniteElement("DG", mesh.ufl_cell(), k - 1)

        Wbar_e_2_H12 = ufl.VectorElement("CG", mesh.ufl_cell(), k)["facet"]
        # Wbar_e_2_H12 = ufl.VectorElement("DGT", mesh.ufl_cell(), k)
        Qbar_E = ufl.FiniteElement("DGT", mesh.ufl_cell(), k)
        return W_e_2, Q_E, Wbar_e_2_H12, Qbar_E

    def function_space(self, mesh: dolfin.Mesh):
        W_e_2, Q_E, Wbar_e_2_H12, Qbar_E = self.stokes_elements(mesh)
        mixedL = dolfin.FunctionSpace(mesh, ufl.MixedElement([W_e_2, Q_E]))
        mixedG = dolfin.FunctionSpace(
            mesh, ufl.MixedElement([Wbar_e_2_H12, Qbar_E]))
        return mixedL, mixedG

    def velocity_sub_space(self, W: dolfin.FunctionSpace):
        return W[0].sub(0)

    def velocity_function_space(self, mesh):
        return dolfin.FunctionSpace(mesh, self.stokes_elements(mesh)[0])

    def pressure_function_space(self, mesh):
        return dolfin.FunctionSpace(mesh, self.stokes_elements(mesh)[1])

    def get_velocity(self, U: dolfin.Function):
        return U[0].sub(0)

    def get_pressure(self, U: dolfin.Function):
        return U[0].sub(1)

    def solution_variable(self, W: dolfin.FunctionSpace):
        return dolfin.Function(W[0]), dolfin.Function(W[1])

    def forms(self, W: dolfin.FunctionSpace, U: dolfin.Function,
              weak_bcs: typing.Sequence[dolfin_dg.operators.DGBC],
              model: StokesModel):
        mesh = W[0].mesh()

        k = self.k

        mixedL = W[0]
        mixedG = W[1]

        alpha = dolfin.Constant(6 * k ** 2)
        forms_generator = leopart.FormsStokes(mesh, mixedL, mixedG, alpha)
        ufl_forms = forms_generator.ufl_forms(model.eta, model.f)

        v, q, vbar, qbar = forms_generator.test_functions()

        for bc in weak_bcs:
            if isinstance(bc, dolfin_dg.DGNeumannBC):
                ufl_forms['S_S'] += dot(bc.get_function(),
                                        vbar) * bc.get_boundary()

        forms_stokes = forms_generator.fem_forms(ufl_forms['A_S'],
                                                 ufl_forms['G_S'],
                                                 ufl_forms['G_ST'],
                                                 ufl_forms['B_S'],
                                                 ufl_forms['Q_S'],
                                                 ufl_forms['S_S'])

        strong_bcs = [dolfin.DirichletBC(mixedG.sub(0),
                                         bc.get_function(),
                                         bc.get_boundary().subdomain_data(),
                                         bc.get_boundary().subdomain_id())
                      for bc in weak_bcs
                      if isinstance(bc, dolfin_dg.DGDirichletBC)]
        strong_bcs += [dolfin.DirichletBC(mixedG.sub(0).sub(bc.component),
                                          dolfin.Constant(0.0),
                                          bc.get_boundary().subdomain_data(),
                                          bc.get_boundary().subdomain_id())
                       for bc in weak_bcs if
                       isinstance(bc, dolfin_dg.DGDirichletNormalBC)]

        return forms_stokes, strong_bcs

    def compute_cfl_dt(self, u_vec, hmin, c_cfl):
        max_u_vec = u_vec.vector().norm("linf")
        return c_cfl * hmin / max_u_vec

    def assemble(self, forms, mats, bcs):
        pass

    def solve_stokes(self, W, U, mats, weak_bcs, model):
        mesh = W[0].mesh()
        forms, strong_bcs = self.forms(W, U, weak_bcs, model)
        ssc = leopart.StokesStaticCondensation(mesh,
                                               forms['A_S'], forms['G_S'],
                                               forms['B_S'],
                                               forms['Q_S'], forms['S_S'])

        ssc.assemble_global_system(True)
        for bc in strong_bcs:
            ssc.apply_boundary(bc)

        Uh, Uhbar = U
        ssc.solve_problem(Uhbar.cpp_object(), Uh.cpp_object(), "mumps",
                          "default")


class HDG2(HDG):
    k = 2
